const DOMObj = {
    removeClass: function(selector, removeClassName) {
        const elementList = document.querySelectorAll(selector);
        for (let i = 0; i < elementList.length; i++) {
            elementList[i].classList.remove(removeClassName);
        }
    }
}
