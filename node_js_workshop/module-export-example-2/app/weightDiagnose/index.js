const diagnoseText = require('./diagnoseText');
const diagnose = require('./diagnose');
const calcWeightIndex = require('./calcweightIndex');

const weight = {
    diagnoseText: diagnoseText,
    diagnose: diagnose, 
    calc: calcWeightIndex
}

module.exports = weight;