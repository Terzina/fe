const stopwatch = {
    _time: 0,
    timerId: 0,
    container: document.getElementById('time'),
    start : function () {
        console.log(this);
        this.updateTime();
        this.timerId = setInterval(()=>{
            this._time++;
            this.updateTime();
        }, 1000);

    },
    stop: function () {
        clearInterval(this.timerId);
    },

    reset: function () {
        this.stop();
        this._time = 0;
        this.updateTime();
    },

    updateTime: function(){
        this.container.textContent = this._time;

    },
    setTime: function (newTime) {
        if(Number.isInteger(newTime)&& newTime>= 0){
            this._time = newTime;
            return {
                status: 'success'
            }
        }
        else {
            return  {
                status: "error",
                message: "argument must be positive integer"

            }
        }
    },
    getTime: function () {
        return this._time;
    }
}