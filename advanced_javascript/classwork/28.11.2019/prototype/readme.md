### План урока (все пути указаны относительно текущей папки):

1. Проблема дублирования одинаковых методов и свойств (файл example-1.html).
2. Что такое `[[Prototype]]` и зачем он нужен?
3. Установка `[[Prototype]]` c помощью `__proto__`  (файл __proto__example.html).
4. Задача №1 (файл exercice-1.html).
5. `__proto__` массивов и объектов (файл __proto__array-object-example.html).
6. Свойство `.prototype` у функции-конструктора как способ установки прототипа созданного функцией объекта (файл prototype.example).
7. Какое преимущество дает помещение методы в прототипы.
8. Задача №2  (файл exercice-2.html).
9. Задача №3  (файл exercice-3.html).
