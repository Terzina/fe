const {src, dest, series, parallel,  watch} = require('gulp');
const sass = require('gulp-sass');							//sass
const browserSync = require('browser-sync').create();		//runtime watcher and changer
const clean = require('gulp-clean');						//cleaner product directory "dev"
const cleanCSS = require('gulp-clean-css');					//CSS minifier
const rigger = require('gulp-rigger');                      //включение кусков html в другой
const rename = require("gulp-rename");						//rename files after minify
const concat = require('gulp-concat');						//concat for js
const terser = require('gulp-terser');						//minify for js
const autoprefixer = require('gulp-autoprefixer');			//cross-browser compatibility css
const imagemin = require('gulp-imagemin');                  // mini images

const fontsFiles = [										//составляем массив переменних с все файлов шрифтов, для переноса в папку разработки
	'./src/fonts/**.eot',
	'./src/fonts/**.ttf',
	'./src/fonts/**.woff',
	'./src/fonts/**.otf'
];

const imgFiles = [
    './src/img/**/**.jpg',
    './src/img/**/**.png'
];

const paths = {
    html: {
        src: '/src/templates/*.html',
        build: '/dist/'
    },
    styles: {
        src: '/src/scss/**/*.scss',
        build: '/dist/css/'
    },
    scripts: {
        src: '/src/js/**/*.js',
        build: '/dist/js/'
    },
    img: {
        src: 'src/img/*',
        build: '/dist/img/'
    }
};


function cleandev() {										//модуль отчистки папки перед каждой расспаковкой
    return src('./dist', {read: false})
        .pipe(clean())
}

function img() {											//модуль переноса картинок
    return src(imgFiles)
    .pipe(imagemin())
        .pipe(dest('./dist/img'))
}

function fonts () {											//Copy fonts to dir "dev"
    return src(fontsFiles)
        .pipe(dest('./dist/fonts'))
}

function js () {											//Copy fonts to dir "dev"
    return src('./src/js/*.js')
        .pipe(dest('./dist/js'))
}

function scripts () {
    return src('src/js/**/*.js')
        .pipe(terser({											//terser
			toplevel: true
		}))														//minify js
        .pipe(concat('all.js'))									//concat all js files
		.pipe(rename(function (path) {							// function of rename extname for .css
            path.extname = ".min.js";
        }))
        .pipe(dest('./dist/js'))
		.pipe(browserSync.stream());
}

function html() {
    return src("src/templates/index.html")
        .pipe(rigger())
        .pipe(dest('./dist/'))
        .pipe(browserSync.stream());
}


function forSass() {
    return src('./src/scss/*.scss')
        .pipe(sass())
        .pipe(cleanCSS({level: 2}))								// minifyCSS
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],								// для браузеров которые использует 0.1%
			cascade: false
        }))
        .pipe(rename(function (path) {							// function of rename extname for .css
            path.extname = ".min.css";
        }))
        .pipe(dest('./dist/css'))
		.pipe(browserSync.stream());
}

function watch() {
	browserSync.init({											// инструмент для live reload
		server: {
			baseDir: "./"
		}
	});

	watch('./src/**/*.scss', forSass);				// ставим watcher для слежения за изменениями в файлах
	watch('./src/**/*.js', scripts);
}

task('cleandev', cleandev);
task('img', img);
task('scripts', scripts);
task('sass', forSass);
task('html', html);
task('watch', watch);
task('fonts', fonts);
task('js', js);
task('build', gulp.series('cleandev', gulp.parallel(img, fonts, scripts, html, forSass)));
task('dev', gulp.series('build', watch));