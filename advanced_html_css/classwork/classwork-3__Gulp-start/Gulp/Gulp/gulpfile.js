const gulp = require("gulp");
const uglify = require("gulp-uglify");
const sass = require("gulp-sass");

gulp.task('js', function () {
    return gulp.src('src/scripts.js')
    .pipe(uglify())
        .pipe(gulp.dest('build'));
});

gulp.task('sass', function () {
    return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
        .pipe(gulp.dest('build/css'));
});

gulp.task("build", ["js", "sass"]);

/*
const test-gulp = function () {
    console.log("Work");
}
*/
